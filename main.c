/*
 * Main program for the virtual memory project.
 * Make all of your modifications to this file.
 * You may add or rearrange any code or data as you need.
 * The header files page_table.h and disk.h explain
 * how to use the page table and disk interfaces.
 * */

#include "page_table.h"
#include "disk.h"
#include "program.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>

struct disk *disk;
int * frame_table;	//to represent the physmem
const char * kickout_method;
int kick_out = 0;
int disk_reads = 0; //performance counter of disk reads, disk writes, number of page faults
int disk_writes = 0;
int page_faults = 0;
int tie;
int random_kickout = 0;
int cant_do;

void rand_kickout (struct page_table *pt, int page, int num_frames) {
	char *  start_physmem = page_table_get_physmem(pt);
	//implement randomization to pick a page to kick out	
	int r_frame = rand() % num_frames;
	//frame table maps to pages
	//i.e. frame_table[frame] = page (number associated)
	int swap = frame_table[r_frame];	
	//write the data before kicking out the page
	disk_write(disk, swap, &start_physmem[PAGE_SIZE * r_frame]);
	disk_writes++;
	//kick out the page by writing over data
	page_table_set_entry(pt, swap, 0, 0);	//we are clearing out the previous page
	page_table_set_entry(pt, page, r_frame, PROT_READ);	//we are updating our page with the new frame
	frame_table[r_frame] = page;
	printf("\n");
	//Implement disk read
	disk_read(disk, page, &start_physmem[PAGE_SIZE * r_frame]);
	disk_reads++;
	return;
}

void fifo_kickout (struct page_table *pt, int page, int num_frames) {
	char *  start_physmem = page_table_get_physmem(pt);
	//kick out first in	
	int swap = frame_table[kick_out];
	//write the data before kicking out the page
	disk_write(disk, swap, &start_physmem[PAGE_SIZE * kick_out]);
	disk_writes++;	
	//kick out the page by writing over data
	page_table_set_entry(pt, swap, 0, 0);	//we are clearing out the previous page
	page_table_set_entry(pt, page, kick_out, PROT_READ);	//we are updating our page with the new frame
	frame_table[kick_out] = page;
	//increment kickout
	kick_out++;
	if (kick_out == num_frames) {
		kick_out = 0;
	}
	printf("\n");
	//Implement disk read
	disk_read(disk, page, &start_physmem[PAGE_SIZE * kick_out]);
	disk_reads++;
	return;
}

void custom_kickout (struct page_table *pt, int num_frames, int page) {
	char *  start_physmem = page_table_get_physmem(pt);
	int m;
	int lowest_page = -1; //set random value
	int lowest_pbit = 60; //set it equal to a random high number
	int pframe, pbit;
	int kickout_frame = -1;
	int current_page;
	tie = 0; //assume there will be no tie

	for (m = 0; m < num_frames; m++) { //loop through the frame table to find a page with only R bits
		current_page = frame_table[m];	
		page_table_get_entry(pt, current_page, &pframe, &pbit);
		if (lowest_pbit > pbit) { 
			lowest_page = current_page;
			lowest_pbit = pbit;
			kickout_frame = m;
		
		}		
	}
	//determine if there was a tie
	for (m = 0; m < num_frames; m++) {
		current_page = frame_table[m];	
		page_table_get_entry(pt, current_page, &pframe, &pbit);
		if (current_page != lowest_page) {
			if (lowest_pbit == pbit) { 	
				tie = 1;
			}
		}
	}
		
	if (tie == 1) {
		//pick a somewhat randomized page to kick out
		//initialized globally
		kickout_frame = random_kickout;
		lowest_page = frame_table[random_kickout];
		random_kickout++;
		if (random_kickout == num_frames) {
			random_kickout = 0;
		}	
	}
	if (kickout_frame == cant_do) {
		kickout_frame += 1;
		if (kickout_frame == num_frames) {
			random_kickout = 0;
		}
	}			
	//write the data before kicking out the page
	disk_write(disk, lowest_page, &start_physmem[PAGE_SIZE * kickout_frame]);
	disk_writes++;	
	//kick out the page by writing over data
	page_table_set_entry(pt, lowest_page, 0, 0);	//we are clearing out the previous page
	page_table_set_entry(pt, page, kick_out, PROT_READ);	//we are updating our page with the new frame
	frame_table[kickout_frame] = page;
	//assign the variable you can't do twice
	cant_do = kickout_frame;	
	//implement the disk read
	disk_read(disk, page, &start_physmem[PAGE_SIZE * kickout_frame]);
	disk_reads++;
	return;
}
	

//----------------------------------------------------------
void page_fault_handler( struct page_table *pt, int page ) {
	page_faults++;
	printf("\n");
	page_table_print(pt);
	printf("\n");
	//first thing we need to do is identify why we page faulted
	//it could be three main reasons:
	//1. nothing in page initially
	//2. writing to page when it is only set to read
	//3. nothing in page and physical memory is full	

	//#################################
	//DECLARING VARIABLES
	int num_frames = page_table_get_nframes(pt);

	char * start_physmem = page_table_get_physmem(pt);
	int i;
	int frame = -1;
	int found = 0;	//check whether we found a free entry
	int updated = 0;

	//###################################
	//CHECK WHETHER WE HAVE A PG_FAULT DUE TO BLANK PAGE
	printf("Checking pg_fault - blank page\n");
	int pframe, pbit;
	page_table_get_entry(pt, page, &pframe, &pbit);
		
	if (pbit == 0) {	//yes it is due to blank page - save page in mfu_frame_table
		//find a free frame
		printf("Page Blank\n");	
		for (i = 0; i < num_frames; i++) {	//loop over frame table to find free entry
			int mem = frame_table[i];
			if (mem == -1) {
				frame = i;	//frame is the current free frame we just chose
				page_table_set_entry(pt, page, frame, PROT_READ);
				found = 1;
				frame_table[frame] = page;
				updated = 1;
				break;
			}

		}
		if (found == 1) {
			//for disk_read we assume that it updates physical memory
			disk_read(disk, page, &start_physmem[PAGE_SIZE * frame]);
			disk_reads++;
		}
		if (found == 0) {
			if (!strcmp("rand", kickout_method)) {
				rand_kickout(pt, page, num_frames);
				updated = 1;
			}
			else if (!strcmp("fifo", kickout_method)) { 
				fifo_kickout(pt, page, num_frames);
				updated = 1;
			}
			else if (!strcmp("custom", kickout_method)) {
				custom_kickout(pt, num_frames, page);
				updated = 1;
			}
			else {
				printf("Kickout method not recognized");
			}	
		}		
	}

	//###################################
	//CHECK WHETHER WE HAVE A PG_FAULT DUE TO WRITING
	page_table_get_entry(pt, page, &pframe, &pbit);
	printf("checking pg_fault - write bit\n");
	if (updated == 0) {
		if (pbit == PROT_READ) {
			page_table_set_entry(pt, page, pframe, PROT_READ|PROT_WRITE);
			updated = 1;
			printf("add prot_write\n");
			
		}
		else if (pbit == PROT_WRITE) {
			page_table_set_entry(pt, page, pframe, PROT_READ|PROT_WRITE);
			updated = 1;
			printf("add prot_read\n");
		}	
		else {
			printf("No read/write error detected\n");
		}
	}
	//###################################
	//CHECK WHETHER PAGE_FAULT IS DUE TO PHYS MEM FULL
	printf("Checking pg_fault - phys mem full\n");
	found = 0;
	if (updated == 0) {
		for (i = 0; i < num_frames; i++) { 
				int mem = frame_table[i];
				if (mem == -1) {
					found = 1; //phys mem is not full
				}
		}
		if (found == 0) { //phys mem is full
			//Use rand and sort to overwrite
			if (pbit == 0) {
				printf("Physical memory full\n");
				if (!strcmp("rand", kickout_method)) {
					rand_kickout(pt, page, num_frames);
					updated = 1;
				}
				else if (!strcmp("fifo", kickout_method)) { 
					fifo_kickout(pt, page, num_frames);
					updated = 1;
				}	
				else if (!strcmp("custom", kickout_method)) {
					custom_kickout(pt, num_frames, page);
					updated = 1;
				}
				else {
					printf("Kickout method not recognized");
				}		
			}		
		}	
		else {
			printf("Physical memory not full\n");
		}
	}
	page_table_print(pt);
	return;
}

int main( int argc, char *argv[] ) {
	if(argc!=5) {
		printf("use: virtmem <npages> <nframes> <rand|fifo|mfu|custom> <sort|scan|focus>\n");
		return 1;
	}
	int npages = atoi(argv[1]);
	int nframes = atoi(argv[2]);
	const char *program = argv[4];
	kickout_method = argv[3];
	printf("this is the kickout method: %s\n", kickout_method);
	
	//FRAME TABLE	
	frame_table = (int *) malloc(nframes * sizeof(int));
	int k;
	for (k = 0; k<nframes; k++){
		frame_table[k] = -1;	
	}
	disk = disk_open("myvirtualdisk", npages);
	
	if(!disk) {
		fprintf(stderr,"couldn't create virtual disk: %s\n", strerror(errno));
		free(frame_table);
		return 1;
	}


	struct page_table *pt = page_table_create( npages, nframes, page_fault_handler );
	if(!pt) {
		fprintf(stderr,"couldn't create page table: %s\n",strerror(errno));
		return 1;
	}
	char *virtmem = page_table_get_virtmem(pt);
	
		if(!strcmp(program,"sort")) {
		printf("error testing sort\n");
		sort_program(virtmem,npages*PAGE_SIZE);

	} else if(!strcmp(program,"scan")) {
		scan_program(virtmem,npages*PAGE_SIZE);

	} else if(!strcmp(program,"focus")) {
		focus_program(virtmem,npages*PAGE_SIZE);

	} else {
		fprintf(stderr,"unknown program: %s\n",argv[3]);
		return 1;
	}
	printf("############## Final metrics  ###############\n");
	printf("Num disk reads: %d\n", disk_reads);
	printf("Num disk writes: %d\n", disk_writes);
	printf("Num page faults: %d\n", page_faults);	
	free(frame_table);
	page_table_delete(pt);
	disk_close(disk);

	return 0;
}
